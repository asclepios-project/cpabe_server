FROM openjdk:8u171-jdk-slim

COPY ./app/target/app-*.jar /cpabe_server/
COPY ./cpabe/cpabe-api/target/cpabe-api-*.jar /cpabe-api/
COPY ./cpabe/cpabe-demo/target/cpabe-demo-*.jar /cpabe-demo/
COPY ./cpabe/cpabe-demo/src/main/resources cpabe/cpabe-demo/src/main/resources
COPY ./policy /policy

COPY ./cpabe/demo/cpabe /cpabe/demo/cpabe

CMD ["java", "-jar", "/cpabe_server/app-1.0-SNAPSHOT.jar", "--spring.profiles.active=production"]
