# Cpabe_server API documentation

This document lists all API endpoints of the `Cpabe_server` application including examples.

[[_TOC_]]

### Disclaimer
**Very important!!!** The `master_key` and `pub_key` files need to be the same in all the components that contain cpabe (Registration Authority, Cpabe_server) in order for the keys to be compatible. The 2 files need to be in the cpabe/demo/cpabe folder, unless specified differently in the `application.properties`.

### Contact

- Project: https://gitlab.com/asclepios-project/cpabe_server
- Maintainer: https://gitlab.com/ikokostas


# Save keys to KeyTray

This endpoint saves the 2 provided keys as a pair in KeyTray and returns the UUID assigned to it.

- URL: `http://localhost:8084/api/v1/put`
- Body: `{ enkcey: “…..”, verkey: “…….”}`
- Format: Json
- Authorization: Bearer Token (The Keycloak Access token from your app)
- Return type: UUID as String

### Example

```text
curl --location \
     --request POST 'http://localhost:8084/api/v1/put' \
     --header 'Authorization: Bearer 
Keycloak_Token' \
     --header 'Content-Type: application/json' \
     --data-raw '{
"encKey": "ujfojfijaopja9387982y98u98jsojnoa08fjua0u",
"verKey": "jijiuufu84789208fnusu0ufn0j0j00js0iuuppsu"
}'
```


# Retrieve and decrypt SSE keys

This endpoint retrieves the 2 SSE keys and tries to decrypt them, returning the 2 decrypted keys if it succeeds.

- URL: `http://localhost:8084/api/v1/get`
- Body: `{ uuid: “…..”, username: “…….”}`
- Format: Json
- Authorization: Bearer Token (The Keycloak Access token from your app)
- Return type: Json `{"encKey":"……….\n","verKey":"……...\n"}`

### Example 

```text
curl --location \
     --request POST 'http://localhost:8084/api/v1/get' \
     --header 'Authorization: Bearer 
Keycloak_Token' \
     --header 'Content-Type: application/json' \
     --data-raw '{"uuid": "1e28ec11-9180-411a-b85a-033dea8dbe8a",
"username": "testuser"}'
```


# Apply policy

This endpoint applies the specified `My_Policy` to the cpABE Server 

- URL: `http://localhost:8084/api/v1/setpolicy`
- Body: My_Policy
- Format: String
- Authorization: Bearer Token (The Keycloak Access token from your app)
- Return type: String `ABE Policy + My_Policy was set`

### Example

``` text
curl --location \
     --request POST 'http://localhost:8084/api/v1/setpolicy' \
     --header 'Authorization: Bearer 
Keycloak_Token' \
     --header 'Content-Type: application/json' \
     --data-raw '"at1:xaxa at2:yy at3:zz 1of3"'
```


# Load predefined policy

This endpoint loads the predefined policy set in the /policy directory of the Docker container. 

**Important:** Make sure the policy's format is the one used in the above /setpolicy example in order to be compatible with the ABE encryption engine.

- URL: `http://localhost:8084/api/v1/loadpolicy`
- Body: -
- Format: -
- Authorization: Bearer Token (The Keycloak Access token from your app)
- Return type: String `Predefined ABE Policy + Predefined_Policy was set`

### Example 

```text
curl --location --request GET 'http://localhost:8084/api/v1/loadpolicy' \
--header 'Authorization: Bearer KEYCLOAK_TOKEN_HERE'
```

# Return current policy

This endpoint returns the currently enforced ABE policy.

- URL: `http://localhost:8084/api/v1/currentpolicy`
- Body: -
- Format: -
- Authorization: Bearer Token (The Keycloak Access token from your app)
- Return type: String `The current enforced ABE Policy is + Current_Policy`

### Example 

```text
curl --location --request GET 'http://localhost:8084/api/v1/currentpolicy' \
--header 'Authorization: Bearer KEYCLOAK_TOKEN_HERE'
```
