package eu.ubitech.asclepios.service.impl;

import co.junwei.cpabe.Demo;
import co.junwei.cpabe.model.EncSSEKey;
import co.junwei.cpabe.model.SSEDto;
import co.junwei.cpabe.util.Loader;
import com.google.gson.*;
import eu.ubitech.asclepios.model.*;
import eu.ubitech.asclepios.service.SSEService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

@Component
public class SSEServiceImpl implements SSEService {

  @Value("${keycloak.auth-server-url}")
  private String keycloakbaseurl;

  @Value("${keycloak.realm}")
  private String keycloakrealm;

  @Value("${keycloak.resource}")
  private String keycloakclient;

  @Value("${keycloak.credentials.secret}")
  private String keycloaksecret;

  @Value("${cpabe.keytray}")
  private String keytray;

  @Value("${cpabe.mode}")
  private String mode;

  private static final Logger logger = Logger.getLogger(SSEServiceImpl.class.getName());

  @Override
  public void init(Demo demo) {
    if (mode.equals("init")){
      try {
        demo.init();
      }
      catch (Exception e){
        e.printStackTrace();
      }
    }
  }

  @Override
  public Token fetchAccessToken() {
    Token token = new Token();
    String tokenendpoint = keycloakbaseurl + "/realms/"+keycloakrealm+"/protocol/openid-connect/token";
    try {
      //---Step 1---------------------------------------
      logger.info("Fetching access token first ");
      final HttpClient client = new DefaultHttpClient();
      List<NameValuePair> params = new ArrayList<NameValuePair>();
      params.add(new BasicNameValuePair("client_id", keycloakclient));
      params.add(new BasicNameValuePair("client_secret", keycloaksecret));
      params.add(new BasicNameValuePair("grant_type", "client_credentials"));
      HttpPost post = new HttpPost(tokenendpoint);
      post.setEntity(new UrlEncodedFormEntity(params));
      //get the response code
      HttpResponse response;
      response = client.execute(post);
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("Token received successfully");
      }
      String jsonString = EntityUtils.toString(response.getEntity());
      //logger.info("response: " + jsonString);

      ObjectMapper mapper = new ObjectMapper();
      //JSON from file to Object
      token = mapper.readValue(jsonString, Token.class);
    } catch (IOException ex) {
      ex.printStackTrace();
      token = null;
    }
    return token;
  }

  @Override
  public Map<String, String> fetchUserAttributes(Token token, String username) {
    try {
      logger.info("Request to fetch userid ");
      String usersearchendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users?username=" + username;
      final HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(usersearchendpoint);
      get.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      get.setHeader("content-type", "application/json");

      //get the response code
      HttpResponse response;
      response = client.execute(get);
      //logger.info("response: " + response);
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("user-id fetched sucessfully");
        String jsonString = EntityUtils.toString(response.getEntity());
        //logger.info("response: " + jsonString);
        JsonArray jsonret = new JsonParser().parse(jsonString).getAsJsonArray();
        if (jsonret.size() > 0) {
          JsonElement element = jsonret.get(0);
          return new Gson().fromJson(element.getAsJsonObject().getAsJsonObject("attributes").toString(), Map.class);
        }
      }

    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return null;
  }

  @Override
  public byte[] fetchCpabeKey(String username) {
    byte[] decodedkey = null;
    try {
      logger.info("Request to fetch userid ");
      String usersearchendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users?username=" + username;
      final HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(usersearchendpoint);
      Token token = fetchAccessToken();
      get.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      get.setHeader("content-type", "application/json");

      //get the response code
      HttpResponse response;
      response = client.execute(get);
      //logger.info("response: " + response);
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("user-id fetched successfully");
        String jsonString = EntityUtils.toString(response.getEntity());
        //logger.info("response: " + jsonString);
        JsonArray jsonret = new JsonParser().parse(jsonString).getAsJsonArray();
        if (jsonret.size() > 0) {
          JsonElement element = jsonret.get(0);
          JsonObject jsonbobject = element.getAsJsonObject().getAsJsonObject("attributes");
          String cpabe = jsonbobject.get("cpabe").getAsString();
          it.unisa.dia.gas.plaf.jpbc.util.io.Base64.decodeToFile(cpabe, "cpabe/demo/cpabe/recPrvfile");
          decodedkey = Base64.getDecoder().decode(cpabe);
        }
        else{
          System.out.println("User with username "+ username +" not found");
        }
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return decodedkey;
  }

  @Override
  public String test(Demo demo) {
    try {
      /*List<String> keys = demo.encryptKeys("asdaffaaf", "afaggagaga");*/
      String token = demo.fetchTestuserToken().access_token;
/*      Request request = new Request(keys.get(0),keys.get(1),token);
      UUID uuid = postSSEKeys(request, demo);
      demo.cleanup();*/
      return token;

    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public UUID postSSEKeys(PutRequest putRequest, Demo demo) {
    List<String> keys = new ArrayList<>();
    try {
      keys = demo.encryptKeys(putRequest.getEncKey(), putRequest.getVerKey());
    }
    catch (Exception e){
      e.printStackTrace();
    }

    String endpoint = keytray + "/add";
    HttpPost post = new HttpPost(endpoint);
    final HttpClient client = new DefaultHttpClient();
    post.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + putRequest.getToken());
    post.setHeader("content-type", "application/json");
    SSEDto sseDto = new SSEDto(keys.get(0), keys.get(1));
    ObjectMapper mapper = new ObjectMapper();
    try {
      String json = mapper. writeValueAsString(sseDto);
      post.setEntity(new StringEntity(json,"UTF-8"));
      HttpResponse response;
      response = client.execute(post);
      String jsonString = EntityUtils.toString(response.getEntity()).replace("\"","");
      return UUID.fromString(jsonString);
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public List<String> getSSEKeys(UUID uuid, String token, Demo demo) {
    EncSSEKey encSSEKey = getSSEKey(uuid, token);
    try {
      return demo.tryToDecrypt(encSSEKey);
    }
    catch (Exception e){
      e.printStackTrace();
    }
    return null;
}



  private EncSSEKey getSSEKey(UUID uuid, String token){
    String endpoint = keytray+ "/get";
    final HttpClient client = new DefaultHttpClient();
    HttpPost post = new HttpPost(endpoint);
    post.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token);
    post.setHeader("content-type", "application/json");
    post.setEntity(new StringEntity(uuid.toString(),"UTF-8"));
    HttpResponse response;
    try {
      response = client.execute(post);
      if (response.getStatusLine().getStatusCode() == 200) {
        EncSSEKey encSSEKey = new EncSSEKey();
        String jsonString = EntityUtils.toString(response.getEntity());
        JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();

        if (!jsonObject.isJsonNull()){
          if (jsonObject.has("uuid"))
            encSSEKey.setUuid(UUID.fromString(jsonObject.get("uuid").getAsString()));
          if (jsonObject.has("encKey")){
            encSSEKey.setEncencKey(jsonObject.get("encKey").getAsString());
          }
          if (jsonObject.has("verKey")){
            encSSEKey.setEncverKey(jsonObject.get("verKey").getAsString());
          }
        }

        return encSSEKey;}
      if (response.getStatusLine().getStatusCode() == 500){
        System.out.println("Key with uuid: "+ uuid + " not found on KeyTray");
      }
    }
    catch (Exception e){
      e.printStackTrace();
    }
    return null;
}
}

