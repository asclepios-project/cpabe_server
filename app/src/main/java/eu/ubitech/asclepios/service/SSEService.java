package eu.ubitech.asclepios.service;

import co.junwei.cpabe.Demo;
import eu.ubitech.asclepios.model.PutRequest;
import eu.ubitech.asclepios.model.Token;
import org.keycloak.representations.AccessToken;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public interface SSEService {

  Token fetchAccessToken();

  byte[] fetchCpabeKey(String username);

  Map<String,String> fetchUserAttributes(Token token, String username);

  String test(Demo demo);

  UUID postSSEKeys(PutRequest putRequest, Demo demo);

  List<String> getSSEKeys(UUID uuid, String token, Demo demo);

  void init(Demo demo);



}
