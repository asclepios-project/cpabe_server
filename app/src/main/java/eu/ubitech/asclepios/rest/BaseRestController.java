package eu.ubitech.asclepios.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BaseRestController {


  @RequestMapping(value = "/", method = RequestMethod.GET)
  private ResponseEntity retrieveAll() {
    return new ResponseEntity("CPABE Server is up and running. To visit the docs go to https://gitlab.com/asclepios-project/cpabe_server/-/blob/master/README.md", HttpStatus.OK);
  }

}
