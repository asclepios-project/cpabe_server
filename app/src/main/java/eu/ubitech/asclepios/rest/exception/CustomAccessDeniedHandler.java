package eu.ubitech.asclepios.rest.exception;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {


  @Override
  public void handle(
      HttpServletRequest request,
      HttpServletResponse response,
      AccessDeniedException exc) throws IOException {

    KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request
        .getUserPrincipal();
    //String userId = principal.getAccount().getKeycloakSecurityContext().getIdToken().getSubject();
    Set<String> roles = principal.getAccount().getRoles();
    roles.remove("offline_access");
    roles.remove("uma_authorization");

    String message =
        "Your have following role(s): " + roles.toString() + " but still you have not rights here!";
    response.sendError(403, message);

  }

}
