package eu.ubitech.asclepios.rest;


import co.junwei.cpabe.Demo;
import com.google.gson.JsonObject;
import eu.ubitech.asclepios.model.ABEAttribute;
import eu.ubitech.asclepios.model.GetRequest;
import eu.ubitech.asclepios.model.PutRequest;
import eu.ubitech.asclepios.service.SSEService;
import org.keycloak.TokenVerifier;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;


@RestController
@RequestMapping("/api/v1")
public class SSERestController {

  private final SSEService sseService;

  private Demo demo;


  public SSERestController(
      @Qualifier("SSEServiceImpl") SSEService iAuthService) throws Exception {
    this.sseService = iAuthService;
    this.demo = new Demo();
    sseService.init(demo);
  }

  @GetMapping(value = "/test")
  private ResponseEntity test() {
    String token = sseService.test(demo);
    return new ResponseEntity(token, HttpStatus.OK);
  }

  @PostMapping(value = "/put")
  private ResponseEntity putKeys(@RequestBody PutRequest putRequest, @RequestHeader("Authorization") String token) {
    putRequest.setToken(token.replace("Bearer ", ""));
    UUID uuid = sseService.postSSEKeys(putRequest, demo);
    demo.cleanup("put");
    return new ResponseEntity(uuid, HttpStatus.OK);
  }

  @PostMapping(value = "/get")
  private ResponseEntity getKeys(@RequestBody GetRequest getRequest, @RequestHeader("Authorization") String token) {
    token = token.replace("Bearer ", "");
    String username = "";
    AccessToken accessToken = null;

    try {
      accessToken = TokenVerifier.create(token,AccessToken.class).getToken();
      username = accessToken.getPreferredUsername();
    }
    catch (Exception e){
      e.printStackTrace();
    }

    sseService.fetchCpabeKey(username);
    UUID uuid = UUID.fromString(getRequest.getUuid());

    List<String> output = sseService.getSSEKeys(uuid,token,demo);
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("encKey",output.get(0));
    jsonObject.addProperty("verKey",output.get(1));
    demo.cleanup("get");
    return new ResponseEntity(jsonObject.toString(), HttpStatus.OK);
  }

  @GetMapping(value = "/currentpolicy")
  private ResponseEntity currentpolicy(){
    return new ResponseEntity("The current enforced ABE Policy is " + demo.getPolicy(),HttpStatus.OK);
  }

  @GetMapping(value = "/loadpolicy")
  private ResponseEntity loadpolicy(){
    String policyXML = readFile("./policy/ABEPolicy.txt");
    String policy = enforcePolicy(policyXML);
    return new ResponseEntity("Predefined ABE Policy: " + policy + " was set",HttpStatus.OK);
  }

  @PostMapping(value = "/setpolicy")
  private ResponseEntity setpolicy(@RequestBody String recpolicy){
    recpolicy = recpolicy.replace("\"","");
    String policy = enforcePolicy(recpolicy);
    return new ResponseEntity("ABE Policy: " + policy + " was set",HttpStatus.OK);
  }

  private String readFile(String filePath){
    StringBuilder contentBuilder = new StringBuilder();

    try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
    {
      stream.forEach(s -> contentBuilder.append(s).append("\n"));
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }

    return contentBuilder.toString();
  }

  private String enforcePolicy(String policyXML){
    if (policyXML.charAt(0) != '('){
      policyXML = policyXML.replace("\n","");
      demo.setPolicy(policyXML);
      return policyXML;
    }
    Scanner scanner = new Scanner(policyXML);
    int andcounter = 1;
    List<ABEAttribute> abeAttributeList = new ArrayList<>();
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine().replace(" ","");

      //SKip the first and last lines
      if (!(line.equals("(")||line.equals(")"))){
        //Check if the line is an attribute
        if (line.startsWith("(")){
          line = line.replace("(","").replace(")","");
          ABEAttribute abeAttribute = new ABEAttribute(line);
          abeAttributeList.add(abeAttribute);
        }
        //The line is a logical operator
        else {
          if (line.equals("AND"))andcounter++;
        }
      }
    }
    scanner.close();
    String policy = formatPolicy(abeAttributeList, andcounter);
    System.out.println(policy);
    demo.setPolicy(policy);

    return policy;
  }

  private String formatPolicy(List<ABEAttribute> abeAttributeList, int andcounter){
    StringBuilder result = new StringBuilder();
    for (ABEAttribute abeattribute: abeAttributeList) {
      result.append(abeattribute.getName()).append(":").append(abeattribute.getValue()).append(" ");
    }
    result.append(andcounter).append("of").append(abeAttributeList.size());
    return result.toString();
  }

}
