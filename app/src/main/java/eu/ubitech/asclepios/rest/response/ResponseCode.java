package eu.ubitech.asclepios.rest.response;

public enum ResponseCode {

  SUCCESS_GENERIC("Successful operation", "1000"),
  FAILURE_GENERIC("Generic Failure", "2000");

  private final String message;
  private final String code;

  private ResponseCode(String message, String code) {
    this.message = message;
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public String getCode() {
    return code;
  }

}
