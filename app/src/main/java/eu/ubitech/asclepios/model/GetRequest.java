package eu.ubitech.asclepios.model;

public class GetRequest {
    private String uuid;
    private String username;


    public GetRequest() {
    }

    public GetRequest(String uuid, String username) {
        this.uuid = uuid;
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
