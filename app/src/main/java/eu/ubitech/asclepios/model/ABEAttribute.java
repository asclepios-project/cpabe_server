package eu.ubitech.asclepios.model;

public class ABEAttribute {

    private String name;
    private String value;

    public ABEAttribute(String line) {
        String[] parts = line.split("=");
        this.name = parts[0];
        this.value = parts[1];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
