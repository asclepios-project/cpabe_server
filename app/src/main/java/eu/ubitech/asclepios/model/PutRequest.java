package eu.ubitech.asclepios.model;

public class PutRequest {
    private String encKey;
    private String verKey;
    private String token;

    public PutRequest() {
    }

    public PutRequest(String encKey, String verKey, String token) {
        this.encKey = encKey;
        this.verKey = verKey;
        this.token = token;
    }

    public String getEncKey() {
        return encKey;
    }

    public void setEncKey(String encKey) {
        this.encKey = encKey;
    }

    public String getVerKey() {
        return verKey;
    }

    public void setVerKey(String verKey) {
        this.verKey = verKey;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
