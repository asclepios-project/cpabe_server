# CpAbe Server Component
The main function of his component is to act as an intermediary between the SSE Client and KeyTray.
It implements two distinct flows:
1. It receives a pair of unencrypted SSE Keys, encrypts them with CpAbe based on the user's ABE Key and stores them in KeyTray, returning the uuid assigned to the pair.
2. It requests a pair of encrypted SSE Keys by using the uuid, tries to decrypt them using the user's ABE Key and if successful, returns the unencrypted pair of SSE Keys

The user's ABE Key is retrieved after contacting Keycloak. A prerequisite is that the user has already registered to the ASCLEPIOS ecosystem via the Registration Authority component, so that his/her ABE key has been already generated.

## Development

To run the component locally:

```$shell
$ docker-compose up -d
```

To run a local instance of KeyTray and manually run the CpAbe_server:

```$shell
$ docker-compose -f docker-compose-development.yaml up -d
```

By default the component is connected to the Keycloak server at https://asclepios-auth.euprojects.net

This, along with the Keycloak Realm and Client can be changed by the following values in the docker-compose:
 
```shell
KEYCLOAK_URL
KEYCLOAK_REALM
KEYCLOAK_CLIENT
KEYCLOAK_SECRET
```
KeyTray is also connected by default to the KeyTray installation provided at https://asclepios-keytray.euprojects.net. This can be changed by the following value in the docker-compose: 
```shell
KEYTRAY_URL
```

## Security
The component is protected by Keycloak authentication, so any requests to it need to contain an Access Token
Please refer to [APIS.odt](APIS.odt) for further instructions on how to call the available APIs securely.

## Important!!
The master_key and pub_key files contained in
```shell
cpabe/demo/cpabe
```
must be the same across all components that utilize cpabe (Registration Authority, CpAbe_Server) to ensure the compatibility of the security keys.