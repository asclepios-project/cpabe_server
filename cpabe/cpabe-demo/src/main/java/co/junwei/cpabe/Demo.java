package co.junwei.cpabe;

import co.junwei.cpabe.model.EncSSEKey;
import co.junwei.cpabe.model.Token;
import co.junwei.cpabe.util.Loader;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.unisa.dia.gas.plaf.jpbc.util.io.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class Demo {

    final static boolean DEBUG = true;
    private Cpabe test;

    Loader loader = new Loader("cpabe/cpabe-demo/src/main/resources/appication.properties");
    HashMap<String, String> properties = loader.init();

    String dir = properties.get("dir");

    String pubfile = dir + properties.get("pubfile");
    String mskfile = dir + properties.get("mskfile");

    String recPrvfile = dir + properties.get("recPrvfile");

    String policy = properties.get("policy");

    String SSEencKey = dir + properties.get("SSEencKey");
    String encSSEencKey = dir + "/SSEencKey.cpabe";

    String SSEverKey = dir + properties.get("SSEverKey");
    String encSSEverKey = dir + "/SSEverKey.cpabe";

    String ReceivedSSEencKey = dir + "/ReceivedSSEencKey.cpabe";
    String decReceivedSSEencKey = dir + "/decReceivedSSEencKey";

    String ReceivedSSEverKey = dir + "/ReceivedSSEverKey.cpabe";
    String decReceivedSSEverKey = dir + "/decReceivedSSEverKey";

    public Demo() throws IOException {
        test = new Cpabe();
    }

    public void init() throws Exception{
        println("//start to setup");
        test.setup(pubfile, mskfile);       //Once by ABE Server. One pub/master key for ALL sessions
        println("//end to setup");

    }

    public List<String> encryptKeys( String requestEnc, String requestVer) throws Exception{
        if (requestEnc == null){
            throw new Exception("Failed to correctly receive the encKey from the request");
        }
        if (requestVer == null){
            throw new Exception("Failed to correctly receive the verKey from the request");
        }
        List<String> keys = new ArrayList<>();
        BufferedWriter writerEnc = new BufferedWriter(new FileWriter(SSEencKey));
        writerEnc.write(requestEnc);
        writerEnc.close();

        BufferedWriter writerVer = new BufferedWriter(new FileWriter(SSEverKey));
        writerVer.write(requestVer);
        writerVer.close();

        //Encrypt SSEnecKey providing policy
        println("//start to enc");
        test.enc(pubfile, policy, SSEencKey, encSSEencKey);
        println("//end to enc");

        //Encrypt SSEnverKey providing policy
        println("//start to enc");
        test.enc(pubfile, policy, SSEverKey, encSSEverKey);
        println("//end to enc");

        String encKey = Base64.encodeFromFile(encSSEencKey);
        String verKey = Base64.encodeFromFile(encSSEverKey);
        keys.add(encKey);
        keys.add(verKey);
        return keys;
    }

    public List<String> tryToDecrypt(EncSSEKey encSSEKey) throws Exception{
        List<String> output = new ArrayList<>();


            if (encSSEKey != null) {

                String receivedSSEenc = encSSEKey.getEncencKey();
                String receivedSSEver = encSSEKey.getEncverKey();

                Base64.decodeToFile(receivedSSEenc, ReceivedSSEencKey);
                Base64.decodeToFile(receivedSSEver, ReceivedSSEverKey);

                //Attempt to Decrypt SSEencKey for User1
                println("//start to dec SSEencKey");
                test.dec(pubfile, recPrvfile, ReceivedSSEencKey, decReceivedSSEencKey);         //Encfile are protected by ABAC rules
                println("//end to dec SSEencKey");

                //Attempt to Decrypt SSEverKey for User1
                println("//start to dec SSEverKey");
                test.dec(pubfile, recPrvfile, ReceivedSSEverKey, decReceivedSSEverKey);         //Encfile are protected by ABAC rules
                println("//end to dec SSEverKey");

                output.add(readFile(decReceivedSSEencKey).replace("\n",""));
                output.add(readFile(decReceivedSSEverKey).replace("\n",""));
                return output;

            } else System.out.println("Error during key retrieval - Key possibly null");

        return null;
    }

    public void setPolicy(String policy){
        this.policy = policy;
    }

    public void cleanup(String request){
        if (request.equals("put")){
            File file = new File(SSEverKey);
            file.delete();
            file = new File(SSEencKey);
            file.delete();
            file = new File(encSSEverKey);
            file.delete();
            file = new File(encSSEencKey);
            file.delete();
        }
        else if (request.equals("get")){
            File file = new File(ReceivedSSEencKey);
            file.delete();
            file = new File(ReceivedSSEverKey);
            file.delete();
            file = new File(decReceivedSSEencKey);
            file.delete();
            file = new File(decReceivedSSEverKey);
            file.delete();
            file = new File(recPrvfile);
            file.delete();
        }
    }

    private static void println(Object o) {
        if (DEBUG) {
            System.out.println(o);
        }
    }

    public Token fetchTestuserToken() {
        Token token;
        String tokenendpoint = System.getenv("KEYCLOAK_URL") + "/realms/" + System.getenv("KEYCLOAK_REALM") + "/protocol/openid-connect/token";
        try {
            //---Step 1---------------------------------------

            final HttpClient client = new DefaultHttpClient();
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("client_id", System.getenv("KEYCLOAK_CLIENT")));
            params.add(new BasicNameValuePair("client_secret", System.getenv("KEYCLOAK_SECRET")));
            params.add(new BasicNameValuePair("username", "testuser"));
            params.add(new BasicNameValuePair("password", "1"));
            params.add(new BasicNameValuePair("grant_type", "password"));
            HttpPost post = new HttpPost(tokenendpoint);
            post.setHeader("content-type", "application/x-www-form-urlencoded");
            post.setEntity(new UrlEncodedFormEntity(params));
            //get the response code
            HttpResponse response;
            response = client.execute(post);
            String jsonString = EntityUtils.toString(response.getEntity());
            //logger.info("response: " + jsonString);

            ObjectMapper mapper = new ObjectMapper();
            //JSON from file to Object
            token = mapper.readValue(jsonString, Token.class);
        } catch (IOException ex) {
            ex.printStackTrace();
            token = null;
        }
        return token;
    }

    private static String readFile(String filePath){
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return contentBuilder.toString();
    }

    public String getPolicy() {
        return policy;
    }
}
