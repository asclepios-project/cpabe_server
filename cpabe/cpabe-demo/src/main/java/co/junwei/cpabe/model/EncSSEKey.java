package co.junwei.cpabe.model;

import java.util.UUID;

public class EncSSEKey {
    private UUID uuid;
    private String EncencKey;
    private String EncverKey;

    public EncSSEKey() {
    }

    public EncSSEKey(UUID uuid, String encencKey, String encverKey) {
        this.uuid = uuid;
        this.EncencKey = encencKey;
        this.EncverKey = encverKey;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getEncencKey() {
        return EncencKey;
    }

    public void setEncencKey(String encencKey) {
        EncencKey = encencKey;
    }

    public String getEncverKey() {
        return EncverKey;
    }

    public void setEncverKey(String encverKey) {
        EncverKey = encverKey;
    }
}
