package co.junwei.cpabe.model;

public class SSEDto {
    private String encKey;
    private String verKey;

    public SSEDto() {
    }

    public SSEDto(String encKey, String verKey) {
        this.encKey = encKey;
        this.verKey = verKey;
    }

    public String getEncKey() {
        return encKey;
    }

    public void setEncKey(String encKey) {
        this.encKey = encKey;
    }

    public String getVerKey() {
        return verKey;
    }

    public void setVerKey(String verKey) {
        this.verKey = verKey;
    }
}
