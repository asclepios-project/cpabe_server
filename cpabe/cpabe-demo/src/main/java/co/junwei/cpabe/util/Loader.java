package co.junwei.cpabe.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

public class Loader {
    private String appConfigPath;

    public Loader(String path) {
        this.appConfigPath = path;
    }

    public HashMap<String, String> init() throws IOException {
        HashMap<String, String> atts = new HashMap<String, String>();
        Properties appProps = new Properties();
        appProps.load(new FileInputStream(appConfigPath));
        appProps.forEach((key, value) -> atts.put(key.toString(), value.toString())) ;
        return atts;
    }
}
